﻿namespace TripPlanner.Domain.UnitTests.Entities
{
    using System;
    using System.Collections.Generic;
    using Builders;
    using Domain.Entities;
    using NUnit.Framework;

    [TestFixture]
    public class TouristAttractionTests
    {
        private TouristAttractionBuilder builder;

        [SetUp]
        public void SetUp()
        {
            this.builder = new TouristAttractionBuilder();
        }

        [Test]
        public void Tourist_attraction_can_be_assigned_with_only_one_category()
        {
            // Arrange
            TouristAttraction place = this.builder;
            Category category = new Category { Name = "Parks" };

            // Act
            place.SetCategory(category);

            // Assert
            Assert.AreEqual("Parks", place.Category.Name);
        }

        [Test]
        public void Category_of_Tourist_Attraction_can_be_changed()
        {
            // Arrange
            TouristAttraction place = this.builder.WithCategory(
                new Category { Name = "Parks" });
            Category secondCategory = new Category { Name = "Monuments" };

            // Act
            place.SetCategory(secondCategory);

            // Assert
            Assert.AreEqual("Monuments", place.Category.Name);
        }

        [Test]
        public void Category_can_be_unlinked_from_Tourist_Attraction()
        {
            // Arrange
            TouristAttraction place = this.builder.WithCategory(
                new Category { Name = "Parks" });

            // Act
            place.UnlinkCategory();

            // Assert 
            Assert.IsNull(place.Category);
        }

        [Test]
        public void Can_calculate_average_rating()
        {
            // Arrange
            TouristAttraction place = this.builder;
            place.AddRating(1, new Rating { Rate = 8 });
            place.AddRating(2, new Rating { Rate = 10 });
            place.AddRating(3, new Rating { Rate = 7 });

            // Act
            double target = place.CalculateAverageRating();

            // Assert
            Assert.AreEqual(8.33, target);
        }

        [Test]
        public void New_rating_summary_is_added_when_its_first_user_rating()
        {
            // Arrange
            TouristAttraction place = this.builder;
            int userID = 1;
            Rating rating = new Rating { Rate = 5 };

            // Act
            place.AddRating(userID, rating);

            // Assert
            Assert.AreEqual(1, place.RatingSummary.Count);
            Assert.AreEqual(5, place.RatingSummary[0].CurrentRate);
        }

        [Test]
        public void Will_update_current_rate_if_its_not_first_user_rating()
        {
            // Arrange
            TouristAttraction place = this.builder;
            int userID = 1;
            Rating firstRating = new Rating { Rate = 5 };
            Rating secondRating = new Rating { Rate = 7 };
            place.AddRating(userID, firstRating);

            // Act
            place.AddRating(userID, secondRating);

            // Assert
            Assert.AreEqual(1, place.RatingSummary.Count);
            Assert.AreEqual(7, place.RatingSummary[0].CurrentRate);
        }

        [Test]
        public void Will_not_change_added_summary_if_its_not_same_user_rating()
        {
            // Arrange
            TouristAttraction place = this.builder;
            int firstUserID = 1;
            int secondUserID = 2;
            Rating firstUserRating = new Rating { Rate = 5 };
            Rating secondUserRating = new Rating { Rate = 7 };
            place.AddRating(firstUserID, firstUserRating);

            // Act 
            place.AddRating(secondUserID, secondUserRating);

            // Arrange 
            Assert.AreEqual(2, place.RatingSummary.Count);
            Assert.AreNotEqual(7, place.RatingSummary[0].CurrentRate);
        }

        public void Tourist_Attraction_must_have_Coordinates()
        {
            // Arrange 
            double latitude = 1.2345;
            double longitude = -5.4321;

            // Act
            TouristAttraction place = this.builder.WithCoordinates(latitude, longitude);

            // Assert
            Assert.AreEqual(1.2345, place.Coordinates.Latitude);
            Assert.AreEqual(-5.4321, place.Coordinates.Longitude);
        }

        [Test]
        public void Coordinates_of_Tourist_Attraction_can_be_changed()
        {
            // Arrange
            TouristAttraction place = this.builder.WithCoordinates(1.2345, -5.4321);
            Coordinates secondCoordinates = new Coordinates();
            secondCoordinates.SetLatitude(-1.2345);
            secondCoordinates.SetLongitude(5.4321);

            // Act
            place.ChangeCoordinates(secondCoordinates);

            // Assert
            Assert.AreEqual(-1.2345, place.Coordinates.Latitude);
            Assert.AreEqual(5.4321, place.Coordinates.Longitude);
        }

        [Test]
        public void Tourist_attraction_can_be_assigned_with_only_one_address()
        {
            // Arrange
            TouristAttraction place = this.builder;
            Address address = new Address { Street = "Sezame Street 6/9" };

            // Act
            place.SetAddress(address);

            // Assert
            Assert.AreEqual("Sezame Street 6/9", place.Address.Street);
        }

        [Test]
        public void Address_of_Tourist_Attraction_can_be_changed()
        {
            // Arrange
            TouristAttraction place = this.builder.WithAddress(new Address { Street = "Kuźnicza 8/44" });
            Address secondAddress = new Address { Street = "Rynek 37" };

            // Act
            place.SetAddress(secondAddress);

            // Assert
            Assert.AreEqual("Rynek 37", place.Address.Street);
        }

        [Test]
        public void Address_can_be_unlinked_from_Tourist_Attraction()
        {
            // Arrange
            TouristAttraction place = this.builder.WithAddress(
                new Address { Street = "Kuźnicza 8/44" });

            // Act
            place.UnlinkAddress();

            // Assert 
            Assert.IsNull(place.Address);
        }

        [Test]
        public void Tourist_Attraction_can_be_assigned_with_only_one_base_price()
        {
            // Arrange
            var place = this.builder.Build();
            decimal basePrice = 12.12m; 

            // Act
            place.SetBasePrice(basePrice);

            // Assert 
            Assert.AreEqual(12.12m, place.Price.BasePrice);
        }

        [Test]
        public void Tourist_Attraction_can_offer_many_discounts()
        {
            // Arrange
            var place = this.builder.Build();
            place.SetBasePrice(12.12m);
            List<Discount> discounts = new List<Discount>
            {
                new Discount { Price = 10.0m, Description = "50% discount" },
                new Discount { Price = 5.0m, Description = "75% discount" },
                new Discount { Price = 0.0m, Description = "100% discount" }
            };

            // Act
            place.SetDiscountPrices(discounts);

            // Assert
            Assert.AreEqual(10.0m, place.Price.Discounts[0].Price);
            Assert.AreEqual(5.0m, place.Price.Discounts[1].Price);
            Assert.AreEqual(0.0m, place.Price.Discounts[2].Price);
        }

        [Test]
        public void Discounts_can_not_be_applied_without_defined_base_price()
        {
            // Arrange 
            var place = this.builder.Build();
            List<Discount> discounts = new List<Discount>
            {
                new Discount { Price = 10.0m, Description = "50% discount" },
                new Discount { Price = 5.0m, Description = "75% discount" },
                new Discount { Price = 0.0m, Description = "100% discount" }
            };

            // Act, Assert
            Assert.Throws<InvalidOperationException>(() => place.SetDiscountPrices(discounts));
        }

        [Test]
        public void Discounts_can_not_be_higher_than_base_price()
        {
            // Arrange
            var place = this.builder.Build();
            place.SetBasePrice(12.12m);
            List<Discount> discounts = new List<Discount>
            {
                new Discount { Price = 12.13m, Description = "50% discount" }
            };

            // Act, Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => place.SetDiscountPrices(discounts));
        }

        [Test]
        public void Can_remove_discounts()
        {
            // Arrange
            var place = this.builder.Build();
            place.SetBasePrice(20.0m);
            List<Discount> discounts = new List<Discount>
            {
                new Discount { Price = 10.0m, Description = "50% discount" },
                new Discount { Price = 5.0m, Description = "75% discount" }
            };
            place.SetDiscountPrices(discounts);

            // Act
            place.RemoveDiscountPrices();

            // Assert
            Assert.IsNull(place.Price.Discounts);
        }

        [Test]
        public void Can_remove_base_price()
        {
            // Arrange
            var place = this.builder.Build();
            place.SetBasePrice(10.0m);

            // Act
            place.RemoveBasePrice();

            // Assert
            Assert.IsNull(place.Price.BasePrice);
        }

        [Test]
        public void Can_not_remove_base_price_with_discounts_defined()
        {
            // Arrange
            var place = this.builder.Build();
            place.SetBasePrice(20.0m);
            List<Discount> discounts = new List<Discount>
            {
                new Discount { Price = 10.0m, Description = "50% discount" },
                new Discount { Price = 5.0m, Description = "75% discount" }
            };
            place.SetDiscountPrices(discounts);

            // Act, Assert
            Assert.Throws<InvalidOperationException>(() => place.RemoveBasePrice());
        }

        [Test]
        public void Can_add_new_discount_entry()
        {
            // Arrange
            var place = this.builder.Build();
            place.SetBasePrice(20.0m);
            List<Discount> discounts = new List<Discount>
            {
                new Discount { Price = 10.0m, Description = "50% discount" }
            };
            place.SetDiscountPrices(discounts);
            Discount newDiscount = new Discount { Price = 5.0m, Description = "75% discount" };

            // Act
            place.AddNewDiscountEntry(newDiscount);

            // Assert
            Assert.AreEqual(2, place.Price.Discounts.Count);
            Assert.AreEqual(5.0m, place.Price.Discounts[1].Price);
        }

        [Test]
        public void Override_discount_with_the_same_description()
        {
            // Arrange
            var place = this.builder.Build();
            place.SetBasePrice(20.0m);
            List<Discount> discounts = new List<Discount>
            {
                new Discount { Price = 10.0m, Description = "50% discount" },
                new Discount { Price = 5.0m, Description = "75% discount" }
            };
            place.SetDiscountPrices(discounts);
            Discount newDiscount = new Discount { Price = 7.5m, Description = "75% discount" };

            // Act
            place.AddNewDiscountEntry(newDiscount);

            // Assert
            Assert.AreEqual(2, place.Price.Discounts.Count);
            Assert.AreEqual(7.5m, place.Price.Discounts[1].Price);
        }

        [Test]
        public void Can_remove_discount_entry()
        {
            // Arrange 
            var place = this.builder.Build();
            place.SetBasePrice(20.0m);
            List<Discount> discounts = new List<Discount>
            {
                new Discount { Price = 10.0m, Description = "50% discount" },
                new Discount { Price = 5.0m, Description = "75% discount" },
                new Discount { Price = 0.0m, Description = "100% discount" }
            };
            place.SetDiscountPrices(discounts);
            int indexToRemove = 1;

            // Act
            place.RemoveDiscountEntry(indexToRemove);

            // Assert
            Assert.AreEqual(2, place.Price.Discounts.Count);
            Assert.AreEqual(0.0m, place.Price.Discounts[1].Price);
        }

        [Test]
        public void Can_not_create_Tourist_Attraction_with_empty_name()
        {
            // Arrange
            string name = "";

            // Act, Assert
            Assert.Throws<ArgumentException>(
                () => this.builder.WithName(name).Build());
        }

        [Test]
        public void Can_not_set_empty_name()
        {
            // Arrange 
            var place = this.builder.Build();
            string name = "";

            // Act, Assert
            Assert.Throws<ArgumentException>(() => place.SetName(name));
        }

        [Test]
        public void Can_assign_opening_hours_to_day_correctly()
        {
            // Arrange
            TouristAttraction place = this.builder.Build();
            Day day = new DayBuilder().WithOpenHour(8)
                                      .WithCloseHour(18);

            // Act
            place.OpeningHours.SetMonday(day);

            // Assert
            Assert.IsNotNull(place.OpeningHours.Monday);
        }

        [Test]
        public void Is_opened_when_between_opening_hours()
        {
            // Arrange
            TouristAttraction place = this.builder.Build();

            DateTime time = new DateTimeBuilder().WithHour(15);

            Day day = new DayBuilder().WithOpenHour(8)
                                      .WithCloseHour(18);

            place.OpeningHours.SetMonday(day);

            // Act
            bool result = place.IsOpened(time);

            // Assert
            Assert.AreEqual(true, result);
        }

        [Test]
        public void Is_opened_when_closing_hour_is_hour_next_day()
        {
            // Arrange
            TouristAttraction place = this.builder.Build();

            DateTime time = new DateTimeBuilder().WithHour(0);

            Day day = new DayBuilder().WithOpenHour(18)
                                      .WithCloseHour(8);

            place.OpeningHours.SetMonday(day);

            // Act
            bool result = place.IsOpened(time);

            // Assert
            Assert.AreEqual(true, result);
        }

        [Test]
        public void Is_opened_when_specific_day_is_not_assigned()
        {
            // Arrange
            TouristAttraction place = this.builder.Build();

            DateTime time = new DateTimeBuilder().WithHour(1);

            // Act
            bool result = place.IsOpened(time);

            // Assert
            Assert.AreEqual(true, result);          
        }

        [Test]
        public void Is_closed_when_not_between_opening_hours()
        {
            // Arrange
            TouristAttraction place = this.builder.Build();

            DateTime time = new DateTimeBuilder().WithHour(20);

            Day day = new DayBuilder().WithOpenHour(8)
                                      .WithCloseHour(18);

            place.OpeningHours.SetMonday(day);

            // Act
            bool result = place.IsOpened(time);

            // Assert
            Assert.AreEqual(false, result);
        }

        [Test]
        public void Checks_minutes_correctly_when_time_hour_equals_opening_hour()
        {
            // Arrange
            TouristAttraction place = this.builder.Build();

            DateTime time = new DateTimeBuilder().WithHour(8)
                                                 .WithMinute(30);

            Day day = new DayBuilder().WithOpenHour(8)
                                      .WithCloseHour(18)
                                      .WithOpenMinute(0);

            place.OpeningHours.SetMonday(day);

            // Act
            bool result = place.IsOpened(time);

            // Assert         
            Assert.AreEqual(true, result);
        }

        [Test]
        public void Checks_minutes_correctly_when_time_hour_equals_closing_hour()
        {
            // Arrange
            TouristAttraction place = this.builder.Build();

            DateTime time = new DateTimeBuilder().WithHour(3)
                                                 .WithMinute(10);

            Day day = new DayBuilder().WithOpenHour(20)
                                      .WithCloseHour(5)
                                      .WithCloseMinute(0);

            place.OpeningHours.SetMonday(day);

            // Act
            bool result = place.IsOpened(time);

            // Assert         
            Assert.AreEqual(true, result);
        }

        [Test]
        public void New_Tourist_Attraction_has_status_set_as_unchecked()
        {
            // Arrange 
            TouristAttraction place = this.builder.Build();

            // Act, Assert
            Assert.AreEqual("Unchecked", place.Status);
        }

        [Test]
        public void Accept_method_change_Tourist_Attraction_status_to_accepted()
        {
            // Arrange
            TouristAttraction place = this.builder.Build();
            
            // Act
            place.AcceptTouristAttraction();

            // Assert 
            Assert.AreEqual("Accepted", place.Status);
        }

        [Test]
        public void Decline_method_change_Tourist_Attraction_status_to_declined()
        {
            // Arrange
            TouristAttraction place = this.builder.Build();

            // Act
            place.DeclineTouristAttraction();

            // Assert 
            Assert.AreEqual("Declined", place.Status);
        }
    }
}