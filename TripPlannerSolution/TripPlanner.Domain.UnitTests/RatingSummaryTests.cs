﻿namespace TripPlanner.Domain.UnitTests
{
    using FakeItEasy;
    using NUnit.Framework;
    using System.Collections.Generic;

    [TestFixture]
    public class RatingSummaryTests
    {
        [Test]
        public void Can_add_new_rating()
        {
            // Arrange
            var summary = new RatingSummary(A.Dummy<int>());
            Rating rating = new Rating { Rate = 5 };

            // Act
            summary.AddNewRatingEntry(rating);

            // Assert
            Assert.AreEqual(1, summary.Ratings.Count);
            Assert.AreEqual(5, summary.Ratings[0].Rate);
        }

        [Test]
        public void Current_rate_is_last_rate_subbmited()
        {
            // Arrange
            var summary = new RatingSummary(A.Dummy<int>());
            Rating firstRating = new Rating { Rate = 5 };
            Rating secondRating = new Rating { Rate = 10 };
            summary.AddNewRatingEntry(firstRating);

            // Act
            summary.AddNewRatingEntry(secondRating);

            // Assert
            Assert.AreEqual(10, summary.CurrentRate);
        }
    }
}
