﻿namespace TripPlanner.Domain.UnitTests.Builders
{
    using System.Collections.Generic;
    using TripPlanner.Domain.Entities;

    internal class TouristAttractionBuilder
    {
        private int id = 1;
        private string name = "default";
        private Coordinates coordinates = new Coordinates();
        private Address address = new Address { Street = "Sezame Street 6/9", City = "Breslau" };
        private int numberOfVisits = 0;
        private bool disabledAccess = true;
        private bool adultsOnly = false;
        private bool published = true;
        // TODO: complete this 
        //private Info info = new Info();
        private OpeningHours openingHours = new OpeningHours();
        private List<RatingSummary> ratingSummary = new List<RatingSummary>();
        private Price price = new Price();
        private Category category = new Category();

        public static implicit operator TouristAttraction(TouristAttractionBuilder instance)
        {
            return instance.Build();
        }

        public TouristAttractionBuilder WithId(int id)
        {
            this.id = id;
            return this;
        }

        public TouristAttractionBuilder WithName(string name)
        {
            this.name = name;
            return this;
        }

        public TouristAttractionBuilder WithCoordinates(
            double latitude, double longitude)
        {
            this.coordinates.SetLatitude(latitude);
            this.coordinates.SetLongitude(longitude);
            return this;
        }

        public TouristAttractionBuilder WithAddress(Address address)
        {
            this.address = address;
            return this;
        }
        
        public TouristAttractionBuilder WithNumberOfVisits(int number)
        {
            this.numberOfVisits = number;
            return this;
        }

        public TouristAttractionBuilder WithDisabledAccess(bool disabledAccess)
        {
            this.disabledAccess = disabledAccess;
            return this;
        }

        public TouristAttractionBuilder WithAdultsOnly(bool adultsOnly)
        {
            this.adultsOnly = adultsOnly;
            return this;
        }

        public TouristAttractionBuilder WithPublished(bool published)
        {
            this.published = published;
            return this;
        }

        public TouristAttractionBuilder WithPrice(Price price)
        {
            this.price = price;
            return this;
        }

        public TouristAttractionBuilder WithCategory(Category category)
        {
            this.category = category;
            return this;
        }

        public TouristAttraction Build()
        {
            var attraction = new TouristAttraction(name, coordinates);
            attraction.SetAddress(this.address);

            return attraction;
        }
    }
}
