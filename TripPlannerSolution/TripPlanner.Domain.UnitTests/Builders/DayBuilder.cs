﻿namespace TripPlanner.Domain.UnitTests.Builders
{
    internal class DayBuilder
    {
        private int openHour = 8;
        private int openMinute = 0;
        private int closeHour = 18;
        private int closeMinute = 0;

        public static implicit operator Day(DayBuilder instance)
        {
            return instance.Build();
        }

        public DayBuilder WithOpenHour(int openHour)
        {
            this.openHour = openHour;
            return this;
        }

        public DayBuilder WithOpenMinute(int openMinute)
        {
            this.openMinute = openMinute;
            return this;
        }

        public DayBuilder WithCloseHour(int closeHour)
        {
            this.closeHour = closeHour;
            return this;
        }

        public DayBuilder WithCloseMinute(int closeMinute)
        {
            this.closeMinute = closeMinute;
            return this;
        }

        public Day Build()
        {
            var day = new Day(this.openHour, this.openMinute,
                this.closeHour, this.closeMinute);

            return day;
        }
    }
}
