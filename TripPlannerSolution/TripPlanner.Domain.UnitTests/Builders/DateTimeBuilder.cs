﻿namespace TripPlanner.Domain.UnitTests.Builders
{
    using System;

    internal class DateTimeBuilder
    {
        private int year = 2015;
        private int month = 8;
        private int day = 17;
        private int hour = 13;
        private int minute = 15;
        private int second = 0;

        public static implicit operator DateTime(DateTimeBuilder instance)
        {
            return instance.Build();
        }

        public DateTimeBuilder WithYear(int year)
        {
            this.year = year;
            return this;
        }

        public DateTimeBuilder WithMonth(int month)
        {
            this.month = month;
            return this;
        }

        public DateTimeBuilder WithDay(int day)
        {
            this.day = day;
            return this;
        }

        public DateTimeBuilder WithHour(int hour)
        {
            this.hour = hour;
            return this;
        }

        public DateTimeBuilder WithMinute(int minute)
        {
            this.minute = minute;
            return this;
        }

        public DateTime Build()
        {
            var dateTime = new DateTime(this.year, this.month, 
                this.day, this.hour, this.minute, this.second);
            return dateTime;
        }
    }
}
