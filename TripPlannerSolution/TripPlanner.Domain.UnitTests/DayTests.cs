﻿namespace TripPlanner.Domain.UnitTests
{
    using System;
    using NUnit.Framework;
    using TripPlanner.Domain.UnitTests.Builders;

    [TestFixture]
    public class DayTests
    {
        private DayBuilder builder;

        [SetUp]
        public void SetUp()
        {
            this.builder = new DayBuilder();
        }

        [TestCase(-2)]
        [TestCase(25)]
        public void Hour_set_can_not_be_invalid(int invalidHour)
        {
            // Arrange, Act, Assert
            Assert.Throws<ArgumentOutOfRangeException>(
                () => this.builder.WithOpenHour(invalidHour).Build());
        }

        [TestCase(-22)]
        [TestCase(60)]
        public void Minute_set_can_not_be_invalid(int invalidMinute)
        {
            // Act, Assert
            Assert.Throws<ArgumentOutOfRangeException>(
                () => this.builder.WithOpenMinute(invalidMinute).Build());
        }

        [Test]
        public void Midnight_hour_set_as_24_becomes_0()
        {
            // Arrange
            int midnightHour = 24;

            // Act
            Day day = this.builder.WithCloseHour(midnightHour);

            // Assert
            Assert.AreEqual(0, day.CloseHour);
        }

        [Test]
        public void Midnight_hour_set_as_24_assigned_with_minutes_is_invalid()
        {
            // Arrange
            int invalidHour = 24;
            int invalidMinute = 22;

            // Act, Assert
            Assert.Throws<ArgumentOutOfRangeException>(
                () => this.builder.WithCloseHour(invalidHour)
                                  .WithCloseMinute(invalidMinute)
                                  .Build());
        }
    }
}