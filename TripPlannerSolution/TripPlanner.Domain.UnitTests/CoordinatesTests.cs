﻿namespace TripPlanner.Domain.UnitTests
{
    using NUnit.Framework;
    using System;

    [TestFixture]
    public class CoordinatesTests
    {
        [Test]
        public void Can_set_valid_coordinates()
        {
            // Arrange
            double latitude = 66.6666;
            double longitude = 133.7777;
            var coordinates = new Coordinates();

            // Act
            coordinates.SetLatitude(latitude);
            coordinates.SetLongitude(longitude);

            // Assert
            Assert.AreEqual(66.6666, coordinates.Latitude);
            Assert.AreEqual(133.7777, coordinates.Longitude);
        }

        [Test]
        public void Can_not_create_coordinates_with_invalid_latitude()
        {
            // Arrange
            double latitude = 90.0001;
            var coordinates = new Coordinates();

            // Act, Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => coordinates.SetLatitude(latitude));
        }

        [Test]
        public void Can_not_create_coordinates_with_invalid_longitude()
        {
            // Arrange
            double longitude = -180.0001;
            var coordinates = new Coordinates();

            // Act, Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => coordinates.SetLongitude(longitude));
        }

        [Test]
        public void Negative_180_longitude_becomes_positive_180()
        {
            // Arrange
            double longitude = -180.0;
            var coordinates = new Coordinates();

            // Act
            coordinates.SetLongitude(longitude);

            // Assert
            Assert.AreEqual(180.0, coordinates.Longitude);
        }
    }
}
