﻿namespace TripPlanner.WebAPI.UnitTests.Models
{
    using System.Collections.ObjectModel;
    using System.Linq;

    using TripPlanner.Domain;
    using TripPlanner.Domain.Entities;

    using TripPlannerWebAPI.Models;

    public class TestAttractionRepository : ITouristAttractionRepository
    {
        private Collection<TouristAttraction> attractions;

        public TestAttractionRepository()
        {
            this.attractions = new Collection<TouristAttraction>();

            var category = new Category() { ID = 1, Name = "Strip Club" };

            var attraction = 
                new TouristAttraction("Cocomo", new Coordinates()) { ID = 1 };
            attraction.SetCategory(category);
            this.attractions.Add(attraction);

            attraction =
                new TouristAttraction("Princess", new Coordinates()) { ID = 2 };
            attraction.SetCategory(category);
            this.attractions.Add(attraction);

            attraction =
                new TouristAttraction("Le Secret", new Coordinates()) { ID = 3 };
            attraction.SetCategory(category);
            this.attractions.Add(attraction);
        }

        public TouristAttraction GetOneById(int id)
        {
            return this.attractions.FirstOrDefault(a => a.ID == id);
        }

        public Collection<TouristAttraction> GetAll()
        {
            return this.attractions;
        }

        public void Add(TouristAttraction attraction)
        {
            if (attraction == null)
            {
                throw new System.ArgumentNullException(nameof(attraction));
            }

            var id = this.attractions.Max(x => x.ID);
            attraction.ID = ++id;

            this.attractions.Add(attraction);
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(TouristAttraction attraction)
        {
            throw new System.NotImplementedException();
        }
    }
}
