﻿namespace TripPlanner.WebAPI.UnitTests.Models
{
    using System.Collections.ObjectModel;
    using System.Linq;

    using TripPlanner.Domain.Entities;

    using TripPlannerWebAPI.Models;

    public class TestCategoryRepository : ICategoryRepository
    {
        private Collection<Category> categories;

        public TestCategoryRepository()
        {
            this.categories = new Collection<Category>()
            {
                new Category() { ID = 1, Name = "Strip Club" }
            };
        }

        public bool IsUniqueCategory(string name)
        {
            return this.categories.All(x => x.Name != name);
        }

        public void Add(Category category)
        {
            if (category == null)
            {
                throw new System.ArgumentNullException(nameof(category));
            }

            var id = 1;

            if (this.categories.Count != 0)
            {
                id = this.categories.Max(x => x.ID);
                ++id;
            }

            category.ID = id;

            this.categories.Add(category);
        }

        public Collection<Category> GetAll()
        {
            return this.categories;
        }

        public Category GetById(int id)
        {
            return this.categories.FirstOrDefault(a => a.ID == id);
        }
    }
}
