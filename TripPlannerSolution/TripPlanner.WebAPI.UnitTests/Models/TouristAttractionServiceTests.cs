﻿namespace TripPlanner.WebAPI.UnitTests.Models
{
    using System.Linq;
    using System.Runtime.InteropServices.ComTypes;

    using Autofac;

    using NUnit.Framework;

    using TripPlanner.Domain;
    using TripPlanner.Domain.Entities;

    using TripPlannerWebAPI.Models;

    [TestFixture]
    public class TouristAttractionServiceTests
    {
        private static TestAttractionRepository attractionRepository;
        private static TestCategoryRepository categoryRepository;

        [SetUp]
        public static void SetUp()
        {
            attractionRepository = new TestAttractionRepository();
            categoryRepository  = new TestCategoryRepository();
        }

    [Test]
        public void Can_get_all_Tourist_Attractions_properly_from_repository()
        {
            // Arrange
            var apiService = new TouristAttractionService(attractionRepository, null, categoryRepository);

            // Act
            var target = apiService.GetAllTouristAttractions();
            
            // Assert
            Assert.AreEqual(3, target.Count());
        }

        [Test]
        public void Can_get_single_Tourist_Attraction_from_repository()
        {
            // Arrange
            var apiService = new TouristAttractionService(attractionRepository, null, categoryRepository);
            const int Id = 1;

            // Act
            var target = apiService.GetTouristAttraction(Id);

            // Assert
            Assert.AreEqual(1, target.ID);
        }

        [Test]
        public void Can_add_Tourist_Attraction_to_repository()
        {
            // Arrange
            var apiService = new TouristAttractionService(attractionRepository, null, categoryRepository);
            var attraction = new TouristAttraction("Sample", new Coordinates());

            // Act
            apiService.AddAttraction(attraction);

            // Assert
            Assert.AreEqual(4, attractionRepository.GetAll().Count);
            Assert.AreEqual("Sample", attractionRepository.GetOneById(4).Name);
        }

        [Test]
        public void If_category_exists_do_not_add_new_one()
        {
            // Arrange
            var apiService = new TouristAttractionService(attractionRepository, null, categoryRepository);
            var attraction = new TouristAttraction("Sample", new Coordinates());
            attraction.SetCategory(new Category() { Name = "Strip Club" });

            // Act
            apiService.AddAttraction(attraction);

            // Assert
            Assert.AreEqual(1, categoryRepository.GetAll().Count);
        }

        [Test]
        public void If_category_does_not_exist_add_new_one()
        {
            // Arrange
            var apiService = new TouristAttractionService(attractionRepository, null, categoryRepository);
            var attraction = new TouristAttraction("Sample", new Coordinates());
            attraction.SetCategory(new Category() { Name = "Sample" });

            // Act
            apiService.AddAttraction(attraction);

            // Assert
            Assert.AreEqual(2, categoryRepository.GetAll().Count);
            Assert.AreEqual("Sample", categoryRepository.GetById(2).Name);
        }
    }
}
