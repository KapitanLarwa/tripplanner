namespace TripPlanner.WebApplication.ViewModels
{
    public class TouristAttractionForListing
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Address Address { get; set; }
    }
}