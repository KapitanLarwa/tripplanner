﻿namespace TripPlanner.WebApplication.ViewModels
{
    using System.ComponentModel;

    public class Category
    {
        [DisplayName("Category name")]
        public string Name { get; set; }
    }
}