﻿namespace TripPlanner.WebApplication.ViewModels
{
    public class OpeningHours
    {
        public Day Monday { get; set; }
        public Day Tuesday { get; set; }
        public Day Wednesday { get; set; }
        public Day Thursday { get; set; }
        public Day Friday { get; set; }
        public Day Saturday { get; set; }
        public Day Sunday { get; set; }

        public bool IsAssignedWithAnyDayValue()
        {
            return this.Monday != null || this.Tuesday != null || this.Wednesday != null || this.Thursday != null
                || this.Friday != null || this.Saturday != null || this.Sunday != null;
        }
    }
}