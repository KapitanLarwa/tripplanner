﻿namespace TripPlanner.WebApplication.ViewModels
{
    public class Day
    {
        public int OpenHour { get; set; }
        public int OpenMinute { get; set; }
        public int CloseHour { get; set; }
        public int CloseMinute { get; set; }

        public string ToViewModelFormatString()
        {
            return string.Format(
                "{0}:{1:D2} - {2}:{3:D2}",
                this.OpenHour,
                this.OpenMinute,
                this.CloseHour,
                this.CloseMinute);
        }
    }
}