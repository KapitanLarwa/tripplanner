﻿namespace TripPlanner.WebApplication.ViewModels
{
    public class Coordinates
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public string ToDecimalDegreesFormatString()
        {
            return this.Latitude.ToString(System.Globalization.CultureInfo.InvariantCulture) 
                + "," + this.Longitude.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }
    }
}