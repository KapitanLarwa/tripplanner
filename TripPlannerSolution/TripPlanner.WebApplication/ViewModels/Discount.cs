﻿namespace TripPlanner.WebApplication.ViewModels
{
    public class Discount
    {
        public decimal Price { get; set; }
        public string Description { get; set; }
    }
}