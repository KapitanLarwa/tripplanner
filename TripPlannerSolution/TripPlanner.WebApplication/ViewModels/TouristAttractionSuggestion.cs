﻿namespace TripPlanner.WebApplication.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class TouristAttractionSuggestion
    {
        [Required(ErrorMessage = "Please enter attraction name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter coordinates")]
        public Coordinates Coordinates { get; set; }

        public Address Address { get; set; }
        public bool DisabledAccess { get; set; }
        public bool AdultsOnly { get; set; }
        public OpeningHours OpeningHours { get; set; }

        public Price Price { get; set; }

        public Category Category { get; set; }
    }
}