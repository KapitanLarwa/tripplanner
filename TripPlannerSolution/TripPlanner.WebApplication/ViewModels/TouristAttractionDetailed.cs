﻿namespace TripPlanner.WebApplication.ViewModels
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class TouristAttractionDetailed
    {
        [HiddenInput(DisplayValue = false)]
        public int ID { get; set; }

        public string Name { get; set; }
        public Coordinates Coordinates { get; set; }
        public Address Address { get; set; }
        public int NumberOfVisits { get; set; }
        public bool DisabledAccess { get; set; }
        public bool AdultsOnly { get; set; }
        public bool Published { get; set; }
        public OpeningHours OpeningHours { get; set; }
        public List<RatingSummary> RatingSummary { get; set; }
        public Price Price { get; set; }
        public Category Category { get; set; }
    }
}