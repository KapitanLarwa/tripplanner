﻿namespace TripPlanner.WebApplication.ViewModels
{
    using System.Collections.Generic;

    public class Price
    {
        public decimal? BasePrice { get; set; }
        public List<Discount> Discounts { get; set; }
    }
}