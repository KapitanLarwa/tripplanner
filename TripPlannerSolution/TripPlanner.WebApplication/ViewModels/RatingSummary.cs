﻿namespace TripPlanner.WebApplication.ViewModels
{
    public class RatingSummary
    {
        public int CurrentRate { get; set; }
    }
}