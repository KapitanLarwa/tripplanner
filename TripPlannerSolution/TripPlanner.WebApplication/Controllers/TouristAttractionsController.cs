﻿namespace TripPlanner.WebApplication.Controllers
{
    using System.Collections.Generic;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using RestSharp;
    using TripPlanner.WebApplication.ViewModels;

    [RoutePrefix("touristattractions")]
    public class TouristAttractionsController : Controller
    {
        [Route]
        public ViewResult GetAllTouristAttractions()
        {
            var client = new RestClient(WebConfigurationManager.AppSettings["webApiUrl"]);
            var request = new RestRequest("api/touristattractions/", Method.GET);
            var queryResult = client.Execute<List<TouristAttractionForListing>>(request).Data;

            return this.View(queryResult);
        }

        [Route("{id}")]
        public ViewResult GetSingleAttraction(int id)
        {
            var client = new RestClient(WebConfigurationManager.AppSettings["webApiUrl"]);
            var request = new RestRequest("api/touristattractions/" + id, Method.GET);
            var queryResult = client.Execute<TouristAttractionDetailed>(request).Data;

            return this.View(queryResult);
        }

        [Route("suggest")]
        public ViewResult SuggestAttraction()
        {
            return this.View();
        }

        [HttpPost]
        [Route("suggest")]
        public ViewResult SuggestAttraction(TouristAttractionSuggestion suggestion)
        {
            if (this.ModelState.IsValid)
            {
                var client = new RestClient(WebConfigurationManager.AppSettings["webApiUrl"]);
                var request = new RestRequest("api/suggestedattractions/add", Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };
                request.AddBody(suggestion);
                client.Execute(request);

                return this.View("thanks");
            }
            else
            {
                return this.View();
            }
        }

        [Route("thanks")]
        public ViewResult Thanks()
        {
            return this.View();
        }
    }
}