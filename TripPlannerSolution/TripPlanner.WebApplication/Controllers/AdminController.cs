﻿namespace TripPlanner.WebApplication.Controllers
{
    using System.Collections.Generic;
    using System.Web.Configuration;
    using System.Web.Mvc;

    using RestSharp;

    using TripPlanner.WebApplication.ViewModels;

    [RoutePrefix("admin")]
    public class AdminController : Controller
    {
        private const string SuggestedAttractionsUrl = "api/suggestedattractions/";

        private readonly RestClient client = new RestClient(WebConfigurationManager.AppSettings["webApiUrl"]);

        public ViewResult Index()
        {
            return this.View();
        }

        [HttpGet]
        [Route("attractions")]
        public ViewResult GetAllSuggestedAttractions()
        {
            var request = new RestRequest(SuggestedAttractionsUrl, Method.GET);
            var queryResult = this.client.Execute<List<TouristAttractionForListing>>(request).Data;

            return this.View(queryResult);
        }

        [HttpGet]
        [Route("attractions/{id}")]
        public ViewResult GetSuggestedAttraction(int id)
        {
            var request = new RestRequest(SuggestedAttractionsUrl + id, Method.GET);
            var queryResult = this.client.Execute<TouristAttractionDetailed>(request).Data;

            return this.View(queryResult);
        }

        [HttpPost]
        [Route("attractions/{id}")]
        public ActionResult Decline(int? id)
        {
            var request = new RestRequest(SuggestedAttractionsUrl + id, Method.DELETE);
            request.AddParameter("ID", id);
            this.client.Execute(request);

            return this.RedirectToAction("GetAllSuggestedAttractions");
        }

        [Route("attractions/edit/{id}")]
        public ViewResult Edit(int id)
        {
            var request = new RestRequest(SuggestedAttractionsUrl + id, Method.GET);
            var queryResult = this.client.Execute<TouristAttractionDetailed>(request).Data;

            return this.View(queryResult);
        }

        [HttpPost]
        [Route("attractions/edit/{id}")]
        public ActionResult Edit(TouristAttractionDetailed attraction)
        {
            if (this.ModelState.IsValid)
            {
                var request = new RestRequest(SuggestedAttractionsUrl + attraction.ID, Method.PUT)
                {
                    RequestFormat = DataFormat.Json
                };
                request.AddBody(attraction);
                this.client.Execute(request);

                return this.RedirectToAction("GetSuggestedAttraction", new { id = attraction.ID });
            }
            else
            {
                return this.View(string.Empty);
            }
        }

        [HttpPost]
        [Route("attractions/accept/{id}")]
        public ActionResult Accept(int? id)
        {
            var request = new RestRequest(SuggestedAttractionsUrl + id, Method.POST);
            this.client.Execute(request);

            return this.RedirectToAction("GetAllSuggestedAttractions");
        }
    }
}