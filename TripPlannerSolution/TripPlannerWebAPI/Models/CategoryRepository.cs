﻿namespace TripPlannerWebAPI.Models
{
    using System.Collections.ObjectModel;
    using System.Linq;

    using TripPlanner.Domain.Entities;

    public class CategoryRepository : ICategoryRepository
    {
        private static readonly Collection<Category> Categories;

        static CategoryRepository()
        {
            Categories = new Collection<Category>();
        }

        public bool IsUniqueCategory(string name)
        {
            return Categories.All(x => x.Name != name);
        }

        public void Add(Category category)
        {
            if (category == null)
            {
                throw new System.ArgumentNullException(nameof(category));
            }

            var id = 1;

            if (Categories.Count != 0)
            {
                id = Categories.Max(x => x.ID);
                ++id;
            }

            category.ID = id;

            Categories.Add(category);
        }

        public Collection<Category> GetAll()
        {
            return Categories;
        }

        public Category GetById(int id)
        {
            return Categories.FirstOrDefault(a => a.ID == id);
        }
    }
}