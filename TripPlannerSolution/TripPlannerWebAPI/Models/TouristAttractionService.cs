﻿namespace TripPlannerWebAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Http;

    using TripPlanner.Domain.Entities;

    public class TouristAttractionService
    {
        private readonly ITouristAttractionRepository repository;
        private readonly ITouristAttractionRepository suggestedRepository;
        private readonly ICategoryRepository categoryRepository;

        public TouristAttractionService(
            ITouristAttractionRepository repository,
            ITouristAttractionRepository suggestedRepository,
            ICategoryRepository categoryRepository)
        {
            this.repository = repository;
            this.suggestedRepository = suggestedRepository;
            this.categoryRepository = categoryRepository;
        }

        public IEnumerable<TouristAttraction> GetAllTouristAttractions()
        {
            return this.repository.GetAll();
        }

        public TouristAttraction GetTouristAttraction(int id)
        {
            var attraction = this.repository.GetOneById(id);
            if (attraction == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            return attraction;
        }

        public void AddAttraction(TouristAttraction attraction)
        {
            if (string.IsNullOrWhiteSpace(attraction.Category.Name))
            {
                attraction.SetCategory(new Category() { Name = "Other" });
            }

            this.repository.Add(attraction);

            if (this.categoryRepository.IsUniqueCategory(attraction.Category.Name))
            {
                this.categoryRepository.Add(attraction.Category);
            }
        }

        public IEnumerable<TouristAttraction> GetAllSuggestedAttractions()
        {
            return this.suggestedRepository.GetAll();
        }

        public TouristAttraction GetSuggestedAttraction(int id)
        {
            var attraction = this.suggestedRepository.GetOneById(id);
            if (attraction == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            return attraction;
        }

        public void AddSuggestedAttraction(TouristAttraction attraction)
        {
            this.suggestedRepository.Add(attraction);
        }

        public void DeleteSuggestedAttraction(int id)
        {
            var attraction = this.GetTouristAttraction(id);
            if (attraction == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            this.suggestedRepository.Delete(id);
        }

        public void PutSuggestedAttraction(TouristAttraction attraction)
        {
            if (!this.suggestedRepository.Update(attraction))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        public IEnumerable<Category> GetAllCategories()
        {
            return this.categoryRepository.GetAll();
        }
    }
}