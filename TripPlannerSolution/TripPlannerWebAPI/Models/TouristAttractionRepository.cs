﻿namespace TripPlannerWebAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using TripPlanner.Domain;
    using TripPlanner.Domain.Entities;

    public class TouristAttractionRepository : ITouristAttractionRepository
    {
        private static Collection<TouristAttraction> attractions;

        static TouristAttractionRepository()
        {
            attractions = new Collection<TouristAttraction>();

            var coordinates = new Coordinates();
            coordinates.SetLatitude(51.109299);
            coordinates.SetLongitude(17.033076);

            var attraction = new TouristAttraction("Cocomo", coordinates)
            {
                ID = 1,
                AdultsOnly = true,
                DisabledAccess = false,
            };
            attraction.SetAddress(new Address()
            {
                Street = "Rynek",
                Number = "37",
                PostCode = "52-443",
                City = "Wroclaw",
                Country = "Poland",
            });

            attraction.OpeningHours.SetAllDays(new Day(20, 0, 5, 0));

            attraction.SetBasePrice(100.0m);
            attraction.SetDiscountPrices(new List<Discount>
            {
                new Discount() { Description = "Students 50%", Price = 50.0m },
                new Discount() { Description = "Combatants 90%", Price = 10.0m }
            });

            attraction.SetNumberOfVisits(1337);

            attraction.SetCategory(new Category() { Name = "Strip Club" });

            attractions.Add(attraction);

            coordinates = new Coordinates();
            coordinates.SetLatitude(51.109299);
            coordinates.SetLongitude(17.033076);
            attraction = new TouristAttraction("Princess", coordinates) { ID = 2 };
            attraction.SetAddress(new Address()
            {
                Street = "Rynek",
                Number = "37",
                PostCode = "52-443",
                City = "Wroclaw",
                Country = "Poland",
            });
            attractions.Add(attraction);

            coordinates = new Coordinates();
            coordinates.SetLatitude(51.111479);
            coordinates.SetLongitude(17.028858);
            attraction = new TouristAttraction("Le Secret", coordinates) { ID = 3 };
            attraction.SetAddress(new Address()
            {
                Street = "Swietego Mikolaja",
                Number = "8",
                PostCode = "50-106",
                City = "Wroclaw",
                Country = "Poland"
            });
            attractions.Add(attraction);
        }

        public TouristAttraction GetOneById(int id)
        {
            return attractions.FirstOrDefault(a => a.ID == id);
        }

        public Collection<TouristAttraction> GetAll()
        {
            return attractions;  
        }

        public void Add(TouristAttraction attraction)
        {
            if (attraction == null)
            {
                throw new System.ArgumentNullException("attraction");
            }

            var newAttraction = new TouristAttraction(attraction);

            var id = attractions.Max(x => x.ID);
            newAttraction.ID = ++id;

            attractions.Add(newAttraction);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(TouristAttraction attraction)
        {
            throw new NotImplementedException();
        }
    }
}