﻿namespace TripPlannerWebAPI.Models
{
    using System.Collections.ObjectModel;

    using TripPlanner.Domain.Entities;

    public interface ICategoryRepository
    {
        bool IsUniqueCategory(string name);
        void Add(Category category);
        Collection<Category> GetAll();
        Category GetById(int id);
    }
}