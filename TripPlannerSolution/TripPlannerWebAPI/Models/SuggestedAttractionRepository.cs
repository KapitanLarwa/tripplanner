﻿namespace TripPlannerWebAPI.Models
{
    using System.Collections.ObjectModel;
    using System.Linq;

    using TripPlanner.Domain.Entities;

    public class SuggestedAttractionRepository : ITouristAttractionRepository
    {
        private static Collection<TouristAttraction> attractions = new Collection<TouristAttraction>();

        public TouristAttraction GetOneById(int id)
        {
            return attractions.FirstOrDefault(a => a.ID == id);
        }

        public Collection<TouristAttraction> GetAll()
        {
            return attractions;
        }

        public void Add(TouristAttraction attraction)
        {
            if (attraction == null)
            {
                throw new System.ArgumentNullException(nameof(attraction));
            }

            if (attractions.Count == 0)
            {
                attraction.ID = 1;
            }
            else
            {
                attraction.ID = attractions.Max(x => x.ID) + 1;
            }

            attractions.Add(attraction);
        }

        public void Delete(int id)
        {
            var attraction = this.GetOneById(id);

            attractions.Remove(attraction);
        }

        public bool Update(TouristAttraction attraction)
        {
            if (attraction == null)
            {
                throw new System.ArgumentNullException(nameof(attraction));
            }

            var temp = attractions.FirstOrDefault(a => a.ID == attraction.ID);
            var index = attractions.IndexOf(temp);

            if (index == -1)
            {
                return false;
            }

            attractions.RemoveAt(index);
            attractions.Add(attraction);
            return true;
        }
    }
}