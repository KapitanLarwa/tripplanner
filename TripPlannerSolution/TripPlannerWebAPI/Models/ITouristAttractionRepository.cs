﻿namespace TripPlannerWebAPI.Models
{
    using System.Collections.ObjectModel;
    using TripPlanner.Domain.Entities;

    public interface ITouristAttractionRepository
    {
        TouristAttraction GetOneById(int id);
        Collection<TouristAttraction> GetAll();
        void Add(TouristAttraction attraction);
        void Delete(int id);
        bool Update(TouristAttraction attraction);
    }
}
