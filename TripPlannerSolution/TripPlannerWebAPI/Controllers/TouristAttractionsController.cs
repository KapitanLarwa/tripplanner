﻿namespace TripPlannerWebAPI.Controllers
{
    using System.Collections.Generic;
    using System.Web.Http;
    using Models;

    using TripPlanner.Domain.Entities;

    public class TouristAttractionsController : ApiController
    {
        private readonly TouristAttractionService touristAttractionService;

        public TouristAttractionsController(TouristAttractionService touristAttractionService)
        {
            this.touristAttractionService = touristAttractionService;
        }

        [HttpGet]
        [Route("api/touristattractions/")]
        public IEnumerable<TouristAttraction> GetAllTouristAttractions()
        {
            return this.touristAttractionService.GetAllTouristAttractions();
        }

        [HttpGet]
        [Route("api/touristattractions/{id}")]
        public TouristAttraction GetTouristAttraction(int id)
        {
            return this.touristAttractionService.GetTouristAttraction(id);
        }

        [HttpPost]
        [Route("api/touristattractions/add")]
        public async void Add()
        {
            var content = await this.Request.Content.ReadAsStringAsync();
            
            var attraction = TouristAttractionCreator.ConvertRequestContentToTouristAttraction(content);

            this.touristAttractionService.AddAttraction(attraction);
        }
    }
}