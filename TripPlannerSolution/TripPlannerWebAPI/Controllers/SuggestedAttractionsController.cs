﻿namespace TripPlannerWebAPI.Controllers
{
    using System.Collections.Generic;
    using System.Web.Http;

    using TripPlanner.Domain.Entities;

    using TripPlannerWebAPI.Models;

    public class SuggestedAttractionsController : ApiController
    {
        private readonly TouristAttractionService apiService;

        public SuggestedAttractionsController(TouristAttractionService apiService)
        {
            this.apiService = apiService;
        }

        [HttpGet]
        [Route("api/suggestedattractions/")]
        public IEnumerable<TouristAttraction> GetAllTouristAttractions()
        {
            return this.apiService.GetAllSuggestedAttractions();
        }

        [HttpGet]
        [Route("api/suggestedattractions/{id}")]
        public TouristAttraction GetTouristAttraction(int id)
        {
            return this.apiService.GetSuggestedAttraction(id);
        }

        [HttpPost]
        [Route("api/suggestedattractions/add")]
        public async void Add()
        {
            var content = await this.Request.Content.ReadAsStringAsync();

            var attraction = TouristAttractionCreator.ConvertRequestContentToTouristAttraction(content);

            this.apiService.AddSuggestedAttraction(attraction);
        }

        [HttpDelete]
        [Route("api/suggestedattractions/{id}")]
        public void DeleteTouristAttraction(int id)
        {
            this.apiService.DeleteSuggestedAttraction(id);
        }

        [HttpPut]
        [Route("api/suggestedattractions/{id}")]
        public async void PutTouristAttraction(int id)
        {
            var content = await this.Request.Content.ReadAsStringAsync();

            var attraction = TouristAttractionCreator.ConvertRequestContentToTouristAttraction(content);
            attraction.ID = id;

            this.apiService.PutSuggestedAttraction(attraction);
        }

        [HttpPost]
        [Route("api/suggestedattractions/{id}")]
        public void AcceptTouristAttraction(int id)
        {
            var attraction = this.GetTouristAttraction(id);

            this.apiService.AddAttraction(attraction);

            this.DeleteTouristAttraction(id);
        }
    }
}