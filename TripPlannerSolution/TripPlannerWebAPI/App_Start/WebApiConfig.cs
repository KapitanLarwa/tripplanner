﻿namespace TripPlannerWebAPI
{
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Http.Dependencies;

    using Autofac;
    using Autofac.Integration.WebApi;
    using Models;

    using TripPlannerWebAPI.Controllers;

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            Register(config, SetUpDependencyResolver());
        }

        public static void Register(HttpConfiguration config, IDependencyResolver dependencyResolver)
        {
            config.DependencyResolver = dependencyResolver;

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "AdminApi",
                routeTemplate: "api/{namespace}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });
        }

        private static IDependencyResolver SetUpDependencyResolver()
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var apiService = new TouristAttractionService(
                new TouristAttractionRepository(),
                new SuggestedAttractionRepository(),
                new CategoryRepository());

            builder.RegisterType<TouristAttractionService>();

            builder.RegisterType<TouristAttractionRepository>().As<ITouristAttractionRepository>().InstancePerRequest();
            builder.RegisterType<SuggestedAttractionRepository>().As<ITouristAttractionRepository>().InstancePerRequest();
            builder.RegisterType<CategoryRepository>().As<ICategoryRepository>();

            builder.Register(ctx => new TouristAttractionsController(apiService));
            builder.Register(ctx => new SuggestedAttractionsController(apiService));

            var container = builder.Build();

            return new AutofacWebApiDependencyResolver(container);
        }
    }
}