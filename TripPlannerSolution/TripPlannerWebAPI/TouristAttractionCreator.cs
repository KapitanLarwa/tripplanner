﻿namespace TripPlannerWebAPI
{
    using System.Collections.Generic;

    using Newtonsoft.Json.Linq;

    using TripPlanner.Domain;
    using TripPlanner.Domain.Entities;

    public class TouristAttractionCreator
    {
        public static TouristAttraction ConvertRequestContentToTouristAttraction(string contentString)
        {
            dynamic obj = JObject.Parse(contentString);

            var coordinates = new Coordinates();
            coordinates.SetLatitude((double)obj.Coordinates.Latitude);
            coordinates.SetLongitude((double)obj.Coordinates.Longitude);

            var address = new Address()
            {
                Street = obj.Address.Street,
                Number = obj.Address.Number,
                PostCode = obj.Address.PostCode,
                City = obj.Address.City,
                Country = obj.Address.Country
            };

            var basePrice = (decimal?)obj.Price.BasePrice;
            var discounts = (List<Discount>)obj.Price.Discounts;

            var disabledAccess = (bool)obj.DisabledAccess;
            var adultsOnly = (bool)obj.AdultsOnly;

            var category = new Category() { Name = obj.Category.Name };

            var attraction = new TouristAttraction(obj.Name.ToString(), coordinates);
            attraction.ChangeCoordinates(coordinates);
            attraction.SetAddress(address);
            attraction.SetDisabledAccess(disabledAccess);
            attraction.SetAdultsOnly(adultsOnly);
            if (basePrice != null)
            {
                attraction.SetBasePrice(basePrice);
                if (discounts != null)
                {
                    attraction.SetDiscountPrices(discounts);
                }
            }

            attraction.SetCategory(category);

            return attraction;
        }
    }
}