﻿namespace TripPlannerWebAPI.IntegrationTests
{
    using System.Web.Http;
    using System.Web.Http.Dependencies;
    using Autofac;
    using Autofac.Integration.WebApi;
    using Owin;
    using TripPlannerWebAPI.Controllers;
    using TripPlannerWebAPI.IntegrationTests.Models;
    using TripPlannerWebAPI.Models;

    public partial class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var configuration = new HttpConfiguration();

            WebApiConfig.Register(configuration, SetUpDependencyResolver());

            appBuilder.UseWebApi(configuration);
        }

        private static IDependencyResolver SetUpDependencyResolver()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(typeof(TouristAttractionsController).Assembly);

            builder.RegisterType<TouristAttractionService>();
            builder.RegisterType<TestAttractionRepository>().As<ITouristAttractionRepository>()
                .InstancePerRequest();
            builder.RegisterType<CategoryRepository>().As<ICategoryRepository>()
                .InstancePerRequest();

            var container = builder.Build();

            return new AutofacWebApiDependencyResolver(container);
        }
    }
}