﻿namespace TripPlannerWebAPI.IntegrationTests.Models
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using TripPlanner.Domain;
    using TripPlanner.Domain.Entities;
    using TripPlannerWebAPI.Models;

    public class TestAttractionRepository : ITouristAttractionRepository
    {
        private readonly Collection<TouristAttraction> attractions = new Collection<TouristAttraction>
        {
            new TouristAttraction("Cocomo", new Coordinates()) { ID = 1 },
            new TouristAttraction("Princess", new Coordinates()) { ID = 2 },
            new TouristAttraction("Le Secret", new Coordinates()) { ID = 3 }
        };

        public TouristAttraction GetOneById(int id)
        {
            return this.attractions.FirstOrDefault(a => a.ID == id);
        }

        public Collection<TouristAttraction> GetAll()
        {
            return this.attractions;
        }

        public void Add(TouristAttraction attraction)
        {
            if (attraction == null)
            {
                throw new System.ArgumentNullException("attraction");
            }

            this.attractions.Add(attraction);
        }

        public void Delete(int id)
        {
            var attraction = this.GetOneById(id);

            this.attractions.Remove(attraction);
        }

        public bool Update(TouristAttraction attraction)
        {
            if (attraction == null)
            {
                throw new System.ArgumentNullException("attraction");
            }

            var foundAttraction = this.attractions.FirstOrDefault(a => a.ID == attraction.ID);
            var index = this.attractions.IndexOf(foundAttraction);

            if (index == -1)
            {
                return false;
            }

            this.attractions.RemoveAt(index);
            this.attractions.Add(attraction);
            return true;
        }
    }
}