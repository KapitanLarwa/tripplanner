﻿namespace TripPlannerWebAPI.IntegrationTests
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using Microsoft.Owin.Hosting;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using TripPlanner.Domain;
    using TripPlanner.Domain.Entities;

    [TestFixture]
    public class TouristAttractionControllerTests
    {
        private static IDisposable _webApp;
        private const string BaseAddress = "http://localhost:9000/";

        [SetUp]
        public static void SetUp()
        {
            _webApp = WebApp.Start<Startup>(BaseAddress);
        }

        [TearDown]
        public static void TearDown()
        {
            _webApp.Dispose();
        }

        [TestCase("api/touristattractions")]
        [TestCase("api/touristattractions/1")]
        public void Call_to_API_should_return_200_http_status_code(string url)
        {
            // Arrange
            using (var httpClient = new HttpClient())
            {
                var requestUri = new Uri(BaseAddress + url);

                // Act
                var result = httpClient.GetAsync(requestUri).Result;

                // Assert
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            }
        }

        [TestCase("api/touristattractions/-1")]
        public void Call_to_API_should_return_404_http_status_code(string url)
        {
            // Arrange
            using (var httpClient = new HttpClient())
            {
                var requestUri = new Uri(BaseAddress + url);

                // Act
                var result = httpClient.GetAsync(requestUri).Result;

                // Assert
                Assert.AreEqual(HttpStatusCode.NotFound, result.StatusCode);
            }
        }

        [TestCase("api/touristattractions/1", 1)]
        [TestCase("api/touristattractions/2", 2)]
        public void Call_to_API_should_return_known_object_with_corresponding_id(string url, int expectedID)
        {
            // Arrange
            using (var httpClient = new HttpClient())
            {
                var requestUri = new Uri(BaseAddress + url);

                // Act
                var result = httpClient.GetStringAsync(requestUri).Result;
                TouristAttraction attraction = JsonConvert.DeserializeObject<TouristAttraction>(result);

                // Assert
                Assert.AreEqual(expectedID, attraction.ID);
            }
        }

        [Test]
        public void Call_to_API_should_return_list_of_known_objects()
        {
            // Arrange
            using (var httpClient = new HttpClient())
            {
                var requestUri = new Uri(BaseAddress + "api/touristattractions/");

                // Act
                var result = httpClient.GetStringAsync(requestUri).Result;
                List<TouristAttraction> attraction = JsonConvert.DeserializeObject<List<TouristAttraction>>(result);

                // Assert
                Assert.AreEqual(1, attraction[0].ID);
                Assert.AreEqual(2, attraction[1].ID);
            }
        }

        [Test]
        public void Call_to_API_should_fulfill_add_request()
        {
            // Arrange
            using (var httpClient = new HttpClient())
            {
                var requestUri = new Uri(BaseAddress + "api/touristattractions/add");
                var attraction = new TouristAttraction("New", new Coordinates());

                var request = new HttpRequestMessage {RequestUri = requestUri};
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                request.Method = HttpMethod.Post;
                request.Content = new ObjectContent<TouristAttraction>(attraction, new JsonMediaTypeFormatter());

                // Act
                HttpResponseMessage result = httpClient.SendAsync(request).Result;

                // Assert
                Assert.True(result.IsSuccessStatusCode);
                Assert.NotNull(result.Content);
            }
        }

        [TestCase("api/suggestedattractions/1")]
        public void Call_To_API_should_fulfill_delete_request(string url)
        {
            // Arrange
            using (var httpClient = new HttpClient())
            {
                var requestUri = new Uri(BaseAddress + url);

                // Act
                var result = httpClient.DeleteAsync(requestUri).Result;

                // Assert
                Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
            }
        }

        [TestCase("api/suggestedattractions/1", 1)]
        [TestCase("api/suggestedattractions/2", 2)]
        public void Call_To_API_should_fulfill_put_request(string url, int id)
        {
            // Arrange
            using (var httpClient = new HttpClient())
            {
                var requestUri = new Uri(BaseAddress + url);
                var attraction = new TouristAttraction("New", new Coordinates()) { ID = id };

                var context = new ObjectContent<TouristAttraction>(attraction, new JsonMediaTypeFormatter());

                // Act
                var result = httpClient.PutAsync(requestUri, context).Result;

                // Assert
                Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
            }
        }
    }
}