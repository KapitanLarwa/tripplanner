﻿namespace TripPlanner.WebApplication.UnitTests.ViewModels
{
    using NUnit.Framework;
    
    using WebApplication.ViewModels;

    [TestFixture]
    public class CoordinatesTests
    {
        private static readonly object[] CoordinatesCases =
            {
                new object[] { 12.3456, 7.89, "12.3456,7.89" },
                new object[] { 89.765, 43.201, "89.765,43.201" },
            };

        [SetCulture("pl-PL")]
        [TestCaseSource("CoordinatesCases")]
        public void Returns_Decimal_Degrees_Format_Correctly_For_Polish_Culture(double latitude, double longitude, string expected)
        {
            this.AssertDecimalDegreesFormatStringResults(latitude, longitude, expected);
        }

        [SetCulture("en-US")]
        [TestCaseSource("CoordinatesCases")]
        public void Returns_Decimal_Degrees_Format_Correctly_For_American_Culture(double latitude, double longitude, string expected)
        {
            this.AssertDecimalDegreesFormatStringResults(latitude, longitude, expected);
        }

        private void AssertDecimalDegreesFormatStringResults(double latitude, double longitude, string expected)
        {
            // Arrange
            var coordinates = new Coordinates { Latitude = latitude, Longitude = longitude };

            // Act
            string result = coordinates.ToDecimalDegreesFormatString();

            // Assert
            Assert.AreEqual(expected, result);
        }
    }
}
