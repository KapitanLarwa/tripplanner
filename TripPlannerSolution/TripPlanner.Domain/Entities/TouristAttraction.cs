﻿namespace TripPlanner.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Newtonsoft.Json;

    public class TouristAttraction
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Coordinates Coordinates { get; private set; } 
        public Address Address { get; private set; }
        public int NumberOfVisits { get; set; }
        public bool DisabledAccess { get; set; }
        public bool AdultsOnly { get; set; }
        public bool Published { get; set; }
        public Info Info { get; set; }
        public OpeningHours OpeningHours { get; private set; }
        public List<RatingSummary> RatingSummary { get; private set; }
        public Price Price { get; private set; }
        public Category Category { get; private set; }
        public string Status { get; private set; }

        [JsonConstructor]
        public TouristAttraction(string name, Coordinates coordinates)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Provide name", "name");
            }

            this.Name = name;
            this.Coordinates = coordinates;
            this.RatingSummary = new List<RatingSummary>();
            this.Price = new Price();
            this.OpeningHours = new OpeningHours();
            this.Status = "Unchecked";
            this.Category = new Category();
        }

        // TODO Delete when DB comes in
        public TouristAttraction(TouristAttraction previous)
        {
            this.Name = previous.Name;
            this.Address = previous.Address;
            this.Coordinates = previous.Coordinates;
            this.DisabledAccess = previous.DisabledAccess;
            this.AdultsOnly = previous.AdultsOnly;
            this.OpeningHours = previous.OpeningHours;
            this.Price = previous.Price;
            this.Category = previous.Category;
            this.RatingSummary = new List<RatingSummary>();
        }

        public void SetCategory(Category category)
        {
            this.Category = category;
        }

        public void UnlinkCategory()
        {
            this.Category = null; 
        }

        public double CalculateAverageRating()
        {
            double total = this.RatingSummary.Sum(rating => rating.CurrentRate);
            return Math.Round(total / this.RatingSummary.Count, 2);
        }

        public void AddRating(int userID, Rating rating)
        {
            int userIndex = this.RatingSummary.FindIndex(user => user.UserID == userID);
            if (userIndex == -1)
            {
                RatingSummary newSummary = new RatingSummary(userID);
                newSummary.AddNewRatingEntry(rating);
                this.RatingSummary.Add(newSummary);
            }
            else
            {
                this.RatingSummary[userIndex].AddNewRatingEntry(rating);
            }
        }

        public void ChangeCoordinates(Coordinates coordinates)
        {
            this.Coordinates = coordinates;
        }

        public void SetAddress(Address address)
        {
            this.Address = address;
        }

        public void UnlinkAddress()
        {
            this.Address = null;
        }

        public void SetBasePrice(decimal? basePrice)
        {
            this.Price.BasePrice = basePrice;
        }

        public void SetDiscountPrices(List<Discount> discounts)
        {
            if (Price.BasePrice == null)
            {
                throw new InvalidOperationException("Can't Set any discount without defined base price.");
            }

            foreach (Discount d in discounts)
            {
                if (d.Price > Price.BasePrice)
                {
                    throw new ArgumentOutOfRangeException("Can't Set discount bigger than base price.");
                }
            }

            this.Price.Discounts = discounts;
        }

        public void AddNewDiscountEntry(Discount discount)
        {
            int result = Price.Discounts.FindIndex(d => d.Description == discount.Description);
            if (result != -1)
            {
                Price.Discounts[result] = discount;
            }
            else
            {
                Price.Discounts.Add(discount);
            }
        }

        public void RemoveDiscountPrices()
        {
            Price.Discounts = null;
        }

        public void RemoveBasePrice()
        {
            if (Price.Discounts != null)
            {
                throw new InvalidOperationException(
                    "Can't remove base price if there's any discount defined");
            }

            Price.BasePrice = null;
        }

        public void RemoveDiscountEntry(int index)
        {
            Price.Discounts.RemoveAt(index);
        }

        public void SetName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Provide name", "name");
            }

            this.Name = name;
        }

        public void SetNumberOfVisits(int number)
        {
            this.NumberOfVisits = number;
        }

        public void SetDisabledAccess(bool access)
        {
            this.DisabledAccess = access;
        }

        public void SetAdultsOnly(bool adultsOnly)
        {
            this.AdultsOnly = adultsOnly;
        }

        public void SetPublished(bool published)
        {
            this.Published = published;
        }

        public bool IsOpened(DateTime time)
        {
            var day = new Day(0, 0, 0, 0);

            switch (time.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    day = this.OpeningHours.Monday;
                    break;
                case DayOfWeek.Tuesday:
                    day = this.OpeningHours.Tuesday;
                    break;
                case DayOfWeek.Wednesday:
                    day = this.OpeningHours.Wednesday;
                    break;
                case DayOfWeek.Thursday:
                    day = this.OpeningHours.Thursday;
                    break;
                case DayOfWeek.Friday:
                    day = this.OpeningHours.Friday;
                    break;
                case DayOfWeek.Saturday:
                    day = this.OpeningHours.Saturday;
                    break;
                case DayOfWeek.Sunday:
                    day = this.OpeningHours.Sunday;
                    break;
            }

            if (day == null)
            {
                return true;
            }

            var nightAttraction = day.OpenHour > day.CloseHour;

            if (!nightAttraction && time.Hour > day.OpenHour && time.Hour < day.CloseHour)
            {
                return true;
            }

            if (nightAttraction && (time.Hour > day.OpenHour || time.Hour < day.CloseHour))
            {
                return true;
            }

            if (time.Hour == day.OpenHour && time.Minute > day.OpenMinute)
            {
                return true;
            }

            if (time.Hour == day.CloseHour && time.Minute < day.CloseMinute)
            {
                return true;
            }

            return false;
        }

        public void AcceptTouristAttraction()
        {
            this.Status = "Accepted";
        }

        public void DeclineTouristAttraction()
        {
            this.Status = "Declined";
        }
    }
}
