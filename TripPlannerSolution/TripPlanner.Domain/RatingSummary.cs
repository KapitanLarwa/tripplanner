﻿namespace TripPlanner.Domain
{
    using System.Collections.Generic;

    public class RatingSummary
    {
        public int UserID { get; private set; }
        public int CurrentRate { get; private set; }
        public List<Rating> Ratings { get; private set; }

        public RatingSummary(int userId)
        {
            this.UserID = userId;
            this.Ratings = new List<Rating>();
        }

        public void AddNewRatingEntry(Rating rating)
        {
            this.Ratings.Add(rating);
            this.CurrentRate = rating.Rate;
        }
    }
}