﻿namespace TripPlanner.Domain
{
    public class Coordinates
    {
        public double Latitude { get; private set; }

        public double Longitude { get; private set; }

        private bool ValidateLatitude(double latitude)
        {
            if (latitude < -90.0 || latitude > 90.0)
            {
                return false;
            }

            return true;
        }

        private bool ValidateLongitude(double longitude)
        {
            if (longitude < -180.0 || longitude > 180.0)
            {
                return false;
            }

            return true;
        }

        public void SetLatitude(double latitude)
        {
            if (this.ValidateLatitude(latitude))
            {
                this.Latitude = latitude;
            }
            else
            {
                throw new System.ArgumentOutOfRangeException();
            }
        }

        public void SetLongitude(double longitude)
        {        
            if (this.ValidateLongitude(longitude))
            {
                this.Longitude = longitude;
            }
            else
            {
                throw new System.ArgumentOutOfRangeException();
            }

            if (this.Longitude == -180)
            {
                this.Longitude = 180;
            }
        }
    }
}