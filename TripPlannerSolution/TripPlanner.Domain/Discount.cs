﻿namespace TripPlanner.Domain
{
    public class Discount
    {
        public decimal Price { get; set; }
        public string Description { get; set; }
    }
}