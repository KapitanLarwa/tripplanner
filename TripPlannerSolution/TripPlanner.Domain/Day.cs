﻿namespace TripPlanner.Domain
{
    using System;

    public class Day
    {
        public int OpenHour { get; private set; }
        public int OpenMinute { get; private set; }
        public int CloseHour { get; private set; }
        public int CloseMinute { get; private set; }

        public Day(int openHour, int openMinute, int closeHour, int closeMinute)
        {
            this.SetOpen(openHour, openMinute);
            this.SetClose(closeHour, closeMinute);
        }

        private bool IsValidHour(int hour)
        {
            if (hour >= 24 || hour < 0)
            {
                return false;
            }

            return true;
        }

        private bool IsValidMinute(int minute)
        {
            if (minute >= 60 || minute < 0)
            {
                return false;
            }

            return true;
        }

        private void TimeValidation(int hour, int minute)
        {
            if (!this.IsValidHour(hour))
            {
                throw new ArgumentOutOfRangeException("hour", "Hour set is not valid");
            }

            if (!this.IsValidMinute(minute))
            {
                throw new ArgumentOutOfRangeException("minute", "Minutes set are not valid");
            }
        }

        public void SetOpen(int hour, int minute)
        {
            if (hour == 24 && minute == 0)
            {
                hour = 0;
            }

            this.TimeValidation(hour, minute);

            this.OpenHour = hour;
            this.OpenMinute = minute;
        }

        public void SetClose(int hour, int minute)
        {
            if (hour == 24 && minute == 0)
            {
                hour = 0;
            }

            this.TimeValidation(hour, minute);

            this.CloseHour = hour;
            this.CloseMinute = minute;
        }
    }
}