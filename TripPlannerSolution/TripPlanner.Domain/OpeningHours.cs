﻿namespace TripPlanner.Domain
{
    public class OpeningHours
    {
        public Day Monday { get; private set; }
        public Day Tuesday { get; private set; }
        public Day Wednesday { get; private set; }
        public Day Thursday { get; private set; }
        public Day Friday { get; private set; }
        public Day Saturday { get; private set; }
        public Day Sunday { get; private set; }

        public void SetAllDays(Day day)
        {
            this.SetMonday(day);
            this.SetTuesday(day);
            this.SetWednesday(day);
            this.SetThursday(day);
            this.SetFriday(day);
            this.SetSaturday(day);
            this.SetSunday(day);
        }

        public void SetMonday(Day day)
        {
            this.Monday = day;
        }

        public void SetTuesday(Day day)
        {
            this.Tuesday = day;
        }

        public void SetWednesday(Day day)
        {
            this.Wednesday = day;
        }

        public void SetThursday(Day day)
        {
            this.Thursday = day;
        }

        public void SetFriday(Day day)
        {
            this.Friday = day;
        }

        public void SetSaturday(Day day)
        {
            this.Saturday = day;
        }

        public void SetSunday(Day day)
        {
            this.Sunday = day;
        }
    }
}
