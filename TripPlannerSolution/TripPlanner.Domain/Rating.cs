﻿namespace TripPlanner.Domain
{
    using System;

    public class Rating
    {
        public int Rate { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
    }
}